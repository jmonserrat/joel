<?php
$submitted = isset($_GET['nickname']) && isset($_GET['email']) && isset($_GET['password']);
if ($submitted) {
	setcookie('nickname', $_GET['nickname']);
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Bookstore</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.0.11/css/fork-awesome.min.css" integrity="sha256-MGU/JUq/40CFrfxjXb5pZjpoZmxiP2KuICN5ElLFNd8=" crossorigin="anonymous">
		<link rel="stylesheet" href="style.css">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="main.js"></script>
	</head>
	<body>
		<input type="hidden" id="password" value="<?php echo $_GET['password']; ?>">
		<?php if ($submitted): ?>
			<p>Your login info is</p>
			<ul>
				<li><b>nickname</b>: <?php echo $_GET['nickname']; ?></li>
				<li><b>email</b>: <?php echo $_GET['email']; ?></li>
				<li><b>password</b>: <span id="password-container"></span> <i id="show-password" class="fa fa-fw fa-eye" aria-hidden="true"></i></li>
			</ul>
		<?php else: ?>
			<p>You did not submit anything.</p>
		<?php endif; ?>
	</body>
</html>
