function hide_password() {
	$("#password-container").html($("#password").val().replace(/./g,"*"));
}

function show_password() {
	$("#password-container").html($("#password").val());
}

$( document ).ready(function() {

	hide_password();
	
	$("#show-password").mousedown(show_password);
	$("#show-password").mouseup(hide_password);
	$("#show-password").mouseout(hide_password);

});
